package com.blpr.ws.beans;

public class Response {
	private String typeResponse;
	private Object result;
	
	public String getTypeResponse() {
		return typeResponse;
	}
	public void setTypeResponse(String typeResponse) {
		this.typeResponse = typeResponse;
	}
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}
	@Override
	public String toString() {
		return "Response [typeResponse=" + typeResponse + ", result=" + result + "]";
	}
	
	
}
