package com.blpr.ws.services;

import java.io.IOException;
import java.text.ParseException;

import com.blpr.ws.beans.Response;

public interface UpdateService {
	
	Response updateAll() throws ParseException, IOException;
}
