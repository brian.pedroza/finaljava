package com.blpr.ws.process;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.blpr.ws.beans.Day;

public class GetDayInfo {
	
    private static final String FILE_NAME = "Registros_Momentum.xls";
    List<Day> listaDias = new ArrayList<>();
    String newstrHour = null;
    String currentIs = null;
    String strDate;
    String strHour;
	Date newDate;
	Date newHour = null;
	Date currentHour; 
	Date currentDate;
	String newstrDate;
	String Hours;
    
	public List<Day> getAllDays() throws ParseException{
		 
		 System.out.println("Llega a GetInfoDay");
		 try {

	            FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
	            System.out.println("Llega");
	            Workbook workbook = new HSSFWorkbook(excelFile);
	            Sheet datatypeSheet = workbook.getSheetAt(0);
	            Iterator<Row> iterator = datatypeSheet.iterator();
	            iterator.next(); //Para que empiece por Row1
	            
	            Row currentRow = iterator.next();//Empieza en 1
	            while (iterator.hasNext()) {
	            	Day newDay = new Day();
	                Cell currentCell = currentRow.getCell(2);
	            	if (currentCell.getCellTypeEnum() == CellType.STRING) {
	                    currentIs = currentCell.getStringCellValue();
	            	} //Obtener IS
	            	currentCell = currentRow.getCell(4);
	            
	            	if (currentCell.getCellTypeEnum() == CellType.STRING) {
	            
	            		currentDate = new SimpleDateFormat("yyyy-MM-dd").parse(currentCell.getStringCellValue());  //Obten fecha(Date)
	            		currentHour = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(currentCell.getStringCellValue()); 
	            		DateFormat dateFormatCurrentDate = new SimpleDateFormat("yyyy-MM-dd");  
	            		DateFormat dateFormatCurrentHour = new SimpleDateFormat("HH:mm:ss");  
	            		strDate = dateFormatCurrentDate.format(currentDate);
	            		strHour = dateFormatCurrentHour.format(currentHour); //ObtenHour(String)
	            		//Segmento2
	            		
	            		currentRow =  iterator.next();
	            		currentCell = currentRow.getCell(4);
	            		newDate = new SimpleDateFormat("yyyy-MM-dd").parse(currentCell.getStringCellValue());  //Obten fecha(Date)
	            		newstrHour = null;
	            		//SegmentoA
	            		while(newDate.equals(currentDate) && currentIs.equals(currentRow.getCell(2).toString())) {
	            		newHour = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(currentCell.getStringCellValue()); 
	                	newstrHour = dateFormatCurrentHour.format(newHour); //ObtenHour(String)	
	                	if(iterator.hasNext() == false) {
	                	break;
	                		}
	                	currentRow = iterator.next();
	                	currentCell = currentRow.getCell(4);
	                	newDate = new SimpleDateFormat("yyyy-MM-dd").parse(currentCell.getStringCellValue());
	                	
	            		}
	            		
	            		if(!strHour.equals(newstrHour) && newstrHour != null){
	                		newDay.setDatestr(strDate);
	                		newDay.setCout(newstrHour);
	                		newDay.setCin(strHour);
	                		newDay.setIsstr(currentIs);                		
	                		currentHour = new SimpleDateFormat("hh:mm:ss").parse(strHour);
	                		newHour = new SimpleDateFormat("hh:mm:ss").parse(newstrHour);
	                		if(newHour.getHours()== 0) newHour.setHours(12);
	                		if(currentHour.getHours()== 0) currentHour.setHours(12);//Validacion para que al parsear la hora el 12:00 no lo tome como 00:00
	                		
	                        long difference = (newHour.getTime() - currentHour.getTime())/1000;//LA diferencia se obtiene en milisegundos
	                        
	                        if((difference/3600)<10) 
	                        	if(((difference%3600)/60)<10) 
	                        		if(((difference%3600)%60)<10)
	                        			Hours= "0"+(difference/3600)+":0"+((difference%3600)/60)+":0"+((difference%3600)%60);
	                        		else Hours= "0"+(difference/3600)+":0"+((difference%3600)/60)+":"+((difference%3600)%60);
	                        	else if(((difference%3600)%60)<10) 
	                        				Hours= "0"+(difference/3600)+":"+((difference%3600)/60)+":0"+((difference%3600)%60);
	                        			else Hours= "0"+(difference/3600)+":"+((difference%3600)/60)+":"+((difference%3600)%60);
	                        else if(((difference%3600)/60)<10)
	                        		if(((difference%3600)%60)<10) Hours= (difference/3600)+":0"+((difference%3600)/60)+":0"+((difference%3600)%60);
	                        		else Hours= (difference/3600)+":0"+((difference%3600)/60)+":"+((difference%3600)%60);
	                        	else if(((difference%3600)%60)<10) Hours= (difference/3600)+":"+((difference%3600)/60)+":0"+((difference%3600)%60);
	                        		else Hours= (difference/3600)+":"+((difference%3600)/60)+":"+((difference%3600)%60);
	                        	
	                        
	                        newDay.setHours(Hours);
	                        listaDias.add(newDay);
	                	}
	            	}
	            	
	            }
	            return listaDias;
	            
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        	return null;
	        } catch (IOException e) {
	            e.printStackTrace();
	        	return null;
	        }
		
	}
}
