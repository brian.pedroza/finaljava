package com.blpr.ws.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.blpr.ws.beans.Month;

@Repository
public interface MonthRepository extends JpaRepository<Month, Long> {
	Month findFirstByIsstr(String isstr);
	Month findByIsstrAndImes(String isstr, Long month);
	List<Month> findAllByImes(Long month);
}
