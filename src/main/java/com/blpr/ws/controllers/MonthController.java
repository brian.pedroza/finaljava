package com.blpr.ws.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.blpr.ws.beans.Month;
import com.blpr.ws.beans.Response;
import com.blpr.ws.services.MonthService;

@RestController
@RequestMapping("api/v1")
@CrossOrigin(origins = "http://localhost:4200")
public class MonthController {
	
	@Autowired
	private MonthService monthService;
	
	@GetMapping(path =  "/users/{isstr}/{month}", produces = "application/json")
	public Response getByIsAndMonth(@PathVariable String isstr, @PathVariable Long month) {
		System.out.println("isstr: "+isstr+ " month: " + month );
		return monthService.getAllHoursByIsAndMonth(isstr, month);
	}

	@GetMapping(path =  "/period/{month}", produces = "application/json")
	public Response getByMonth(@PathVariable Long month) {
		return monthService.getAllUsersByMonth(month);
	}
}
