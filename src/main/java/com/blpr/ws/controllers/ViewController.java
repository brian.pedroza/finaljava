package com.blpr.ws.controllers;

import java.io.IOException;
import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.blpr.ws.beans.Response;



@Controller
@RequestMapping("api/v2")
public class ViewController {
	
	@Autowired
	private DayController dayController;
	
	@Autowired
	private MonthController monthController;
	
	@Autowired
	private UpdateController updateController;
	
	Response resp;
	
	@GetMapping(path =  "/days")
	public ModelAndView getAllUsers(){
		resp = dayController.getAllUsers();
		if(resp.getTypeResponse().equals("200 Succefully"))
			return new ModelAndView("recordsDay", "days", resp.getResult());
		else return new ModelAndView("notFound");
	}
	
	@PostMapping(path =  "/period")
	public ModelAndView getAllUsersByStrtDateAndEndDate(@ModelAttribute("startDate") String startDate, @ModelAttribute("endDate") String endDate  ){
		resp = dayController.getAllUsersByStrtDateAndEndDate(startDate, endDate);
		if(resp.getTypeResponse().equals("200 Succefully"))
			return new ModelAndView("recordsStartAndEndDay", "daysSdEd", resp.getResult());
		else return new ModelAndView("notFound");
	}
	
	@PostMapping(path =  "/users")
	public ModelAndView getAllRecordsByStrtDateAndEndDateAndIs(@ModelAttribute("startDate") String startDate, @ModelAttribute("endDate") String endDate
			, @ModelAttribute("is") String is
			){
		resp = dayController.getAllRecordsByStrtDateAndEndDateAndIs(startDate, endDate, is);
		if(resp.getTypeResponse().equals("200 Succefully"))
			return new ModelAndView("recordsIsStartDayEndDay", "daysIsSdEd", resp.getResult());
		else return new ModelAndView("notFound");
	}
	
	@GetMapping(path =  "/users")
	public ModelAndView getByIsAndMonth(@ModelAttribute("is") String isstr, @ModelAttribute("month") Long month) {
		resp = monthController.getByIsAndMonth(isstr, month);
		if(resp.getTypeResponse().equals("200 Succefully"))
			return new ModelAndView("recordsIsMonth","monthIsM",resp.getResult());
		else return new ModelAndView("notFound");
	}

	@GetMapping(path =  "/period")
	public ModelAndView getByMonth(@ModelAttribute("month") Long month) {
		resp = monthController.getByMonth(month);
		if(resp.getTypeResponse().equals("200 Succefully"))
			return new ModelAndView("recordsMonth","monthM",resp.getResult());
		else return new ModelAndView("notFound");
	}
	
	@GetMapping(path = "/update")
    public ModelAndView updateInfo() throws ParseException, IOException {
        return new ModelAndView("updateResponse","resp",updateController.updateInfo());
    }
	
    @RequestMapping(path = "/menu")
    public String menu() {
        return "menu";
    }
    @RequestMapping(path = "/startAndEndDay")
    public String startAndEndDay() {
    	return "startAndEndDay";
    }
    
    @RequestMapping(path = "/isStartAndEndDay")
    public String isStartAndEndDay() {
    	return "isStartAndEndDay";
    }
    @RequestMapping(path = "/month")
    public String month() {
    	return "month";
    }
    
    @RequestMapping(path = "/isMonth")
    public String isMonth() {
    	return "isMonth";
    }

}
